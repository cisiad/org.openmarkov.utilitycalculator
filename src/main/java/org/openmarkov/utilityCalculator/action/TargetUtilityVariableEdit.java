/*
* Copyright 2012 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/


package org.openmarkov.utilityCalculator.action;

import javax.swing.undo.CannotUndoException;

import org.openmarkov.core.action.SimplePNEdit;
import org.openmarkov.core.exception.DoEditException;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Variable;

/**
 * 
 * @author Iñigo
 *
 */
@SuppressWarnings("serial")
public class TargetUtilityVariableEdit extends SimplePNEdit
{
    private final String TARGET_UTILITY_VARIABLE = "targetUtility"; 
    private String oldTargetVariableName;
    private String newTargetVariableName;

    public TargetUtilityVariableEdit (ProbNet probNet, Variable targetVariable)
    {
        super (probNet);
        
        newTargetVariableName = targetVariable.getName ();
        oldTargetVariableName = (probNet.additionalProperties.containsKey (TARGET_UTILITY_VARIABLE)) ? 
                                         probNet.additionalProperties.get (TARGET_UTILITY_VARIABLE)
                                         : null;                                                                                                          
    }

    @Override
    public void doEdit ()
        throws DoEditException
    {
        probNet.additionalProperties.put (TARGET_UTILITY_VARIABLE, newTargetVariableName);
    }

    @Override
    public void undo ()
        throws CannotUndoException
    {
        super.undo ();
        
        if(oldTargetVariableName != null)
        {
            probNet.additionalProperties.remove (TARGET_UTILITY_VARIABLE);
        }else{
            probNet.additionalProperties.put (TARGET_UTILITY_VARIABLE, oldTargetVariableName);
        }
    }
    
    
}
