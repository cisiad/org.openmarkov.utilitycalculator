/*
* Copyright 2012 CISIAD, UNED, Spain
*
* Licensed under the European Union Public Licence, version 1.1 (EUPL)
*
* Unless required by applicable law, this code is distributed
* on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
*/


package org.openmarkov.utilityCalculator.action;

import javax.swing.undo.CannotUndoException;

import org.openmarkov.core.action.SimplePNEdit;
import org.openmarkov.core.exception.DoEditException;
import org.openmarkov.core.model.network.ProbNet;

/**
 * 
 * @author Iñigo
 *
 */
@SuppressWarnings("serial")
public class UtilityPenaltyEdit extends SimplePNEdit
{
    private final String UTILITY_PENALTY = "unobservedUtilityPenalty"; 
    private int oldUtilityPenalty;
    private int newUtilityPenalty;

    public UtilityPenaltyEdit (ProbNet probNet, int utilityPenalty)
    {
        super (probNet);
        
        newUtilityPenalty = utilityPenalty;
        oldUtilityPenalty = (probNet.additionalProperties.containsKey (UTILITY_PENALTY)) ? 
                                         Integer.parseInt (probNet.additionalProperties.get (UTILITY_PENALTY))
                                         : -1;                                                                                                         
    }

    @Override
    public void doEdit ()
        throws DoEditException
    {
        probNet.additionalProperties.put (UTILITY_PENALTY, new Integer (newUtilityPenalty).toString ());
    }

    @Override
    public void undo ()
        throws CannotUndoException
    {
        super.undo ();
        
        if(oldUtilityPenalty != -1)
        {
            probNet.additionalProperties.remove (UTILITY_PENALTY);
        }else{
            probNet.additionalProperties.put (UTILITY_PENALTY, new Integer (oldUtilityPenalty).toString ());
        }
    }
    
    
}
