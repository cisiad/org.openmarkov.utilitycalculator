/*
 * Copyright 2012 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.utilityCalculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.InvalidStateException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.inference.annotation.InferenceManager;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.Finding;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.inference.likelihoodWeighting.LikelihoodWeighting;

public class UtilityCalculator {

	
	public class TuningUtilities
	{
		private Map<Variable, Double> utilities;
		private String state;
		private Variable variable;
		
		public Map<Variable, Double> getUtilities() {
			return utilities;
		}

		public Variable getVariable() {
			return variable;
		}
		
		public String getState() {
			return state;
		}
		public TuningUtilities(Map<Variable, Double> utilities, Variable variableToTest, String value) {
			super();
			this.variable = variableToTest;
			this.utilities = utilities;
			this.state = value;
		}
	}	

    public List<Tuning> getUtility(ProbNet probNet,
			Variable utilityVariable, List<Variable> tunableVariables,
			int neutralStateIndex) {

		LikelihoodWeighting inferenceAlgorithm = null;
		List<Tuning> utilities = new ArrayList<Tuning>();
		try {
			inferenceAlgorithm = new LikelihoodWeighting(probNet);
			inferenceAlgorithm.setSampleSize(1000);
		
			for(Variable variableToTest : tunableVariables)
			{
				for(int i= 0; i < variableToTest.getNumStates(); ++i)
				{
					if(i != neutralStateIndex)
					{
						double utility = Double.NEGATIVE_INFINITY;
						try
						{
							EvidenceCase evidence = new EvidenceCase();
							for(Variable otherVariable : tunableVariables)
							{
								try {
									if(!variableToTest.equals(otherVariable))
									{
											evidence.addFinding(new Finding(otherVariable, neutralStateIndex));
									}else
									{
											evidence.addFinding(new Finding(otherVariable, i));
									}
								} catch (InvalidStateException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}					
							}
							inferenceAlgorithm.setPostResolutionEvidence(evidence);
							try {
								utility = inferenceAlgorithm.getProbsAndUtilities().get(utilityVariable).values[0];
							} catch (Exception e) {
								e.printStackTrace();
							} 
						}catch (IncompatibleEvidenceException e) {
							e.printStackTrace();
						}
						utilities.add(new Tuning(utility, variableToTest, variableToTest.getStateName(i)));
					}
				}
			}
		} catch (NotEvaluableNetworkException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Collections.sort(utilities);
		return utilities;
	}
	
	/**
	 * Returns an array of tuning utilities
	 * @param probNet
	 * @param tunableVariables
	 * @param neutralStateIndex
	 * @return
	 */
	public List<TuningUtilities> getUtilities(ProbNet probNet, List<Variable> tunableVariables,
			int neutralStateIndex) {

		InferenceManager inferenceManager = new InferenceManager();
        List<TuningUtilities> tuningUtilities = new ArrayList<UtilityCalculator.TuningUtilities>();
		InferenceAlgorithm inferenceAlgorithm;
        try
        {
            inferenceAlgorithm = inferenceManager.getDefaultInferenceAlgorithm(probNet);
    		
    		for(Variable variableToTest : tunableVariables)
    		{
    			for(int i= 0; i < variableToTest.getNumStates(); ++i)
    			{
    				if(i != neutralStateIndex)
    				{
    					HashMap<Variable,TablePotential>  probsAndUtilities =  null;
    					try
    					{
    						EvidenceCase evidence = new EvidenceCase();
    						for(Variable otherVariable : tunableVariables)
    						{
    							try {
    								if(!variableToTest.equals(otherVariable))
    								{
    										evidence.addFinding(new Finding(otherVariable, neutralStateIndex));
    								}else
    								{
    										evidence.addFinding(new Finding(otherVariable, i));
    								}
    							} catch (InvalidStateException e) {
    								// TODO Auto-generated catch block
    								e.printStackTrace();
    							}
    						}
    						inferenceAlgorithm.setPostResolutionEvidence(evidence);
    						try {
    							probsAndUtilities = inferenceAlgorithm.getProbsAndUtilities();
    						} catch (Exception e) {
    							e.printStackTrace();
    						} 
    					}catch(IncompatibleEvidenceException e)
    					{
    						
    					}
    					HashMap<Variable,Double> utilities = new HashMap<Variable, Double>();
    					for(Node node: probNet.getNodes())
    					{
    						if(node.getNodeType() == NodeType.UTILITY)
    						{
    							if(probsAndUtilities == null)
    							{
    								utilities.put(node.getVariable(), Double.NEGATIVE_INFINITY);
    							}else {
    								utilities.put(node.getVariable(), probsAndUtilities.get(node.getVariable()).values[0]);
    							}
    						}
    					}
    					tuningUtilities.add(new TuningUtilities(utilities, variableToTest, variableToTest.getStateName(i)));
    				}
    			}
    		}
        }
        catch (NotEvaluableNetworkException e1)
        {
            JOptionPane.showMessageDialog(null,"Error during utility calculation. Check message window for details",
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
            e1.printStackTrace();
        }
		
		return tuningUtilities;
	}	



}
