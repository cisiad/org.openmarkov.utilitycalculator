/*
 * Copyright 2012 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.utilityCalculator;

import org.openmarkov.core.model.network.Variable;

public class Tuning implements Comparable<Tuning> {
    private double   utility;
    private Variable variable;
    private String   state;

    public double getUtility() {
        return utility;
    }

    public Variable getVariable() {
        return variable;
    }

    public String getState() {
        return state;
    }

    public Tuning(double utility, Variable variable, String value) {
        super();
        this.utility = utility;
        this.variable = variable;
        this.state = value;
    }

    @Override
    public int compareTo(Tuning arg0) {
        int result = 1;
        if (utility > arg0.getUtility())
            result = -1;
        else if (utility == arg0.getUtility())
            result = 0;
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(variable.getName());
        sb.append("(");
        sb.append(state);
        sb.append(")=");
        sb.append(utility);
        return sb.toString();
    }

}