/*
 * Copyright 2011 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.utilityCalculator;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;

import org.openmarkov.core.exception.CanNotDoEditException;
import org.openmarkov.core.exception.ConstraintViolationException;
import org.openmarkov.core.exception.DoEditException;
import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.InvalidStateException;
import org.openmarkov.core.exception.NonProjectablePotentialException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.exception.WrongCriterionException;
import org.openmarkov.core.gui.dialog.io.FileFilterAll;
import org.openmarkov.core.gui.loader.element.OpenMarkovLogoIcon;
import org.openmarkov.core.gui.plugin.ToolPlugin;
import org.openmarkov.core.gui.window.MainPanel;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.Finding;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.oopn.OOPNet;
import org.openmarkov.utilityCalculator.action.TargetUtilityVariableEdit;
import org.openmarkov.utilityCalculator.action.UtilityPenaltyEdit;

/**
 * GUI for the utility calculator
 * @author fjdiez
 * @author ibermejo
 * @version 1.0
 * @since OpenMarkov 1.0
 */
@SuppressWarnings("serial")
@ToolPlugin(name = "UtilityCalculator", command = "Tools.UtilityCalculator")
public class UtilityCalculatorGUI extends javax.swing.JDialog
{
    private ProbNet                net;
    private ProbNet                originalNet;
    private EvidenceCase           currentEvidence;
    private List<Tuning>           bestSequence;
    private List<Tuning>           nextTunings;
    private BestSequenceCalculator bestSequenceCalculator;
    private Variable               targetUtilityVariable = null;
    private int                    utilityPenalty = -1;
    /**
     * Messages string resource.
     */
    private JFileChooser           reportFileChooser = null;

    /**
     * Constructor for UtilityCalculatorGUI.
     * @param parent
     */
    public UtilityCalculatorGUI (JFrame parent)
    {
        super (parent, true);
        initComponents ();
        setIconImage (OpenMarkovLogoIcon.getUniqueInstance ().getOpenMarkovLogoIconImage16 ());
        try
        {
            UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | UnsupportedLookAndFeelException e)
        {
            e.printStackTrace ();
        }        
        setDefaultCloseOperation (HIDE_ON_CLOSE);
        setLocationRelativeTo (null);
        DefaultTableModel tuningSequenceTableModel = new TuningTableModel ();
        tuningSequenceTable.setModel (tuningSequenceTableModel);
        tuningSequenceTable.getSelectionModel ().setSelectionMode (ListSelectionModel.SINGLE_SELECTION);
        DefaultTableModel nextTuningTableModel = new TuningTableModel ();
        nextTuningTable.setModel (nextTuningTableModel);
        nextTuningTable.getSelectionModel ().setSelectionMode (ListSelectionModel.SINGLE_SELECTION);
        nextTuningTable.addMouseListener (new MouseAdapter ()
            {
                @Override
                public void mouseClicked (MouseEvent e)
                {
                    super.mouseClicked (e);
                    if (e.getClickCount () > 1)
                    {
                        try {
                            applySelectedTuning ();
                        } catch (IncompatibleEvidenceException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }
                }
            });
        if (MainPanel.getUniqueInstance ().getMainPanelListenerAssistant ().getCurrentNetworkPanel () != null)
        {
            originalNet = MainPanel.getUniqueInstance ().getMainPanelListenerAssistant ().getCurrentNetworkPanel ().getProbNet ();
            net = (net instanceof OOPNet) ? ((OOPNet) originalNet).getPlainProbNet () : originalNet;
            currentEvidence = MainPanel.getUniqueInstance ().getMainPanelListenerAssistant ().getCurrentNetworkPanel ().getEditorPanel ().getCurrentEvidenceCase ();
            try
            {
                currentEvidence.fuse (MainPanel.getUniqueInstance ().getMainPanelListenerAssistant ().getCurrentNetworkPanel ().getEditorPanel ().getPreResolutionEvidence (),
                                      true);
            }
            catch (IncompatibleEvidenceException e1)
            {
                e1.printStackTrace ();
            }
            utilityVariableComboBox.removeAllItems ();
            for (Node node : net.getNodes ())
            {
                if (node.getNodeType () == NodeType.UTILITY)
                {
                    utilityVariableComboBox.addItem (node.getName ());
                }
            }
            if (net.additionalProperties.containsKey ("targetUtility") && net.containsVariable (net.additionalProperties.get ("targetUtility")))
            {
                try
                {
                    targetUtilityVariable = net.getVariable (net.additionalProperties.get ("targetUtility"));
                }
                catch (NodeNotFoundException ignore)
                {
                }
                utilityVariableComboBox.setSelectedItem (net.additionalProperties.get ("targetUtility"));
            }
            
            if (net.additionalProperties.containsKey ("unobservedUtilityPenalty"))
            {
                utilityPenalty = Integer.parseInt (net.additionalProperties.get ("unobservedUtilityPenalty"));
                utilityPenaltySpinner.setValue (utilityPenalty);
            }
            else
            {
                utilityPenaltySpinner.setValue (10);
            }
            setVisible (true);
        }
        else
        {
            JOptionPane.showMessageDialog (null, "Open a net first.", "Warning",
                                           JOptionPane.WARNING_MESSAGE);
        }// TODO fail if the open net has no utility nodes
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    // <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        searchTypeButtonGroup = new javax.swing.ButtonGroup();
        jPanel5 = new javax.swing.JPanel();
        runButton = new javax.swing.JButton();
        closeButton = new javax.swing.JButton();
        saveReportButton = new javax.swing.JButton();
        applyTuningSequenceButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        utilityVariableComboBox = new javax.swing.JComboBox<String>();
        jPanel3 = new javax.swing.JPanel();
        automaticSearchRadioButton = new javax.swing.JRadioButton();
        steppedSearchRadioButton = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tuningSequenceTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        lblTotalUtility = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        nextTuningTable = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        utilityPenaltySpinner = new javax.swing.JSpinner();
        jPanel8 = new javax.swing.JPanel();
        thresholdSpinner = new javax.swing.JSpinner();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("OpenMarkov - Utility Calculator");
        setPreferredSize(new java.awt.Dimension(480, 560));

        runButton.setText("Run");
        runButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runButtonActionPerformed(evt);
            }
        });

        closeButton.setText("Close");
        closeButton.setPreferredSize(new java.awt.Dimension(99, 23));
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        saveReportButton.setEnabled(false);
        saveReportButton.setText("Save Report");
        saveReportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveReportButtonActionPerformed(evt);
            }
        });

        applyTuningSequenceButton.setText("Apply Tuning Sequence");
        applyTuningSequenceButton.setToolTipText("");
        applyTuningSequenceButton.setEnabled(false);
        applyTuningSequenceButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                applyTuningSequenceButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .add(6, 6, 6)
                .add(runButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(applyTuningSequenceButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(saveReportButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(closeButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 90, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(11, Short.MAX_VALUE))
        );

        jPanel5Layout.linkSize(new java.awt.Component[] {closeButton, runButton}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(runButton)
                    .add(closeButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(saveReportButton)
                    .add(applyTuningSequenceButton))
                .addContainerGap())
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Utility variable"));

        utilityVariableComboBox.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(utilityVariableComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 162, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(266, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .add(0, 0, Short.MAX_VALUE)
                .add(utilityVariableComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Search type"));

        searchTypeButtonGroup.add(automaticSearchRadioButton);
        automaticSearchRadioButton.setSelected(true);
        automaticSearchRadioButton.setText("Automatic");

        searchTypeButtonGroup.add(steppedSearchRadioButton);
        steppedSearchRadioButton.setText("Step-by-step");

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(automaticSearchRadioButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(steppedSearchRadioButton))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                .add(automaticSearchRadioButton)
                .add(steppedSearchRadioButton))
        );

        steppedSearchRadioButton.getAccessibleContext().setAccessibleName("SetByStep");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Tuning sequence"));

        tuningSequenceTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tuningSequenceTable);

        jLabel1.setText("Total utility:");

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 413, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel4Layout.createSequentialGroup()
                        .add(jLabel1)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(lblTotalUtility)))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(jScrollPane2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 128, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(lblTotalUtility))
                .add(0, 11, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Next tuning"));

        nextTuningTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(nextTuningTable);

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 413, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel6Layout.createSequentialGroup()
                .add(0, 0, Short.MAX_VALUE)
                .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 128, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Unobserved utility penalty"));

        org.jdesktop.layout.GroupLayout jPanel7Layout = new org.jdesktop.layout.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(utilityPenaltySpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 54, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel7Layout.createSequentialGroup()
                .add(0, 0, Short.MAX_VALUE)
                .add(utilityPenaltySpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder("Threshold"));

        thresholdSpinner.setValue(1.0);

        org.jdesktop.layout.GroupLayout jPanel8Layout = new org.jdesktop.layout.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(30, Short.MAX_VALUE)
                .add(thresholdSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 54, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel8Layout.createSequentialGroup()
                .add(0, 0, Short.MAX_VALUE)
                .add(thresholdSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(4, 4, 4)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(layout.createSequentialGroup()
                                .add(jPanel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .add(jPanel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(13, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanel8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void applyTuningSequenceButtonActionPerformed (ActionEvent evt)
    {
        EvidenceCase evidenceCase = MainPanel.getUniqueInstance ().getMainPanelListenerAssistant ().getCurrentNetworkPanel ().getEditorPanel ().getCurrentEvidenceCase ();
        for(Tuning nextTuning : bestSequence)
        {
            try {
                evidenceCase.changeFinding(new Finding(nextTuning.getVariable(),
                                                       nextTuning.getVariable().getStateIndex(nextTuning.getState())));
            } catch (InvalidStateException | IncompatibleEvidenceException notGoingToHappen) {
            }
        }
        this.setVisible (false);
    }    
    private void saveReportButtonActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_saveReportButtonActionPerformed
        reportFileChooser = new JFileChooser ();
        reportFileChooser.setAcceptAllFileFilterUsed (false);
        reportFileChooser.setFileFilter(new FileFilterAll (".xls", "Excel Files (.xls"));
        if (reportFileChooser.showSaveDialog (this) == JFileChooser.APPROVE_OPTION)
        {
            String reportFilename = reportFileChooser.getSelectedFile ().getAbsolutePath ();
            if (!reportFilename.endsWith (".xls"))
            {
                reportFilename += ".xls";
            }
            try
            {
                UtilityReport utilityReport = new UtilityReport ();
                utilityReport.saveTuningList (reportFilename, bestSequence);
            }
            catch (IOException e)
            {
                JOptionPane.showMessageDialog (null, "Error while trying to generate report",
                                               "Error", JOptionPane.ERROR_MESSAGE);
                e.printStackTrace ();
            }
        }
    }// GEN-LAST:event_saveReportButtonActionPerformed

    private void closeButtonActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_cancelButtonActionPerformed
        this.setVisible (false);
    }// GEN-LAST:event_cancelButtonActionPerformed

    private void runButtonActionPerformed (java.awt.event.ActionEvent evt)
    {// GEN-FIRST:event_EvaluateButtonActionPerformed
        int newUtilityPenalty = Integer.parseInt (utilityPenaltySpinner.getValue ().toString ());
        if (newUtilityPenalty <= 0)
        {
            JOptionPane.showMessageDialog (null, "Utility penalty must be higher than zero",
                                           "Error", JOptionPane.ERROR_MESSAGE);
        }
        else
        {
            if(utilityPenalty != newUtilityPenalty)
            {
                UtilityPenaltyEdit edit = new UtilityPenaltyEdit (originalNet, newUtilityPenalty); 
                try
                {
                    originalNet.doEdit(edit);
                }
                catch (ConstraintViolationException
                        | CanNotDoEditException | NonProjectablePotentialException
                        | WrongCriterionException | DoEditException e)
                {
                    e.printStackTrace();
                }
            }
            Variable newTargetUtilityVariable = null;
            try
            {
                newTargetUtilityVariable = net.getVariable (utilityVariableComboBox.getSelectedItem ().toString ());
                if(!newTargetUtilityVariable.equals (targetUtilityVariable))
                {
                    TargetUtilityVariableEdit edit = new TargetUtilityVariableEdit (originalNet, newTargetUtilityVariable);
                    try
                    {
                        originalNet.doEdit (edit);
                    }
                    catch (ConstraintViolationException
                            | CanNotDoEditException | NonProjectablePotentialException
                            | WrongCriterionException | DoEditException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            catch (NodeNotFoundException e)
            {
                // Can not happen
            }
            // Clean tuning sequence table
            DefaultTableModel tuningSequenceTableModel = (DefaultTableModel) tuningSequenceTable.getModel ();
            tuningSequenceTableModel.setRowCount (0);
            // Clean next tunings table
            DefaultTableModel nextTuningTableModel = (DefaultTableModel) nextTuningTable.getModel ();
            nextTuningTableModel.setRowCount (0);
            net = (net instanceof OOPNet) ? ((OOPNet) originalNet).getPlainProbNet () : originalNet;
            List<Variable> tunableVariables = net.getVariables (NodeType.DECISION);
            try
            {
                bestSequenceCalculator = new BestSequenceCalculator (net, tunableVariables,
                                                                     newTargetUtilityVariable,
                                                                     currentEvidence, 1);
                DecimalFormat df = new DecimalFormat ("#.##");
                lblTotalUtility.setText (df.format (bestSequenceCalculator.getGroundUtility ()));
                if (automaticSearchRadioButton.isSelected ())
                {
                    double threshold = Double.parseDouble (thresholdSpinner.getValue ().toString ());
                    BestSequenceSearchTask bestSequenceSearchTask = new BestSequenceSearchTask (bestSequenceCalculator, threshold);
                    bestSequenceSearchTask.execute ();
                }
                else
                {
                    NextTuningsSearchTask nextTuningsSearchTask = new NextTuningsSearchTask (bestSequenceCalculator);
                    nextTuningsSearchTask.execute ();
                }
            }
            catch (IncompatibleEvidenceException e)
            {
                JOptionPane.showMessageDialog (null,
                                               "Ground evidence is incompatible with current evidence",
                                               "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }// GEN-LAST:event_EvaluateButtonActionPerformed

    private void applySelectedTuning () throws IncompatibleEvidenceException
    {
        Tuning selectedTuning = nextTunings.get (nextTuningTable.getSelectedRow ());
        bestSequenceCalculator.applyTuning (selectedTuning);
        DefaultTableModel tableModel = (DefaultTableModel) tuningSequenceTable.getModel ();
        DecimalFormat df = new DecimalFormat ("#.##");
        tableModel.addRow (new Object[] {selectedTuning.getVariable ().getName (),
                selectedTuning.getState (), df.format (selectedTuning.getUtility ())});
        lblTotalUtility.setText (df.format (bestSequenceCalculator.getGroundUtility ()));
        NextTuningsSearchTask nextTuningsSearchTask = new NextTuningsSearchTask (
                                                                                 bestSequenceCalculator);
        nextTuningsSearchTask.execute ();
        ((DefaultTableModel) nextTuningTable.getModel ()).setRowCount (0);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton applyTuningSequenceButton;
    private javax.swing.JRadioButton automaticSearchRadioButton;
    private static javax.swing.JButton closeButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblTotalUtility;
    private javax.swing.JTable nextTuningTable;
    private static javax.swing.JButton runButton;
    private javax.swing.JButton saveReportButton;
    private javax.swing.ButtonGroup searchTypeButtonGroup;
    private javax.swing.JRadioButton steppedSearchRadioButton;
    private javax.swing.JSpinner thresholdSpinner;
    private javax.swing.JTable tuningSequenceTable;
    private javax.swing.JSpinner utilityPenaltySpinner;
    private javax.swing.JComboBox<String> utilityVariableComboBox;
    // End of variables declaration//GEN-END:variables
    private class TuningTableModel extends DefaultTableModel
    {
        public TuningTableModel ()
        {
            super (new String[] {"Variable", "State", "Utility"}, 0);
        }

        @Override
        public boolean isCellEditable (int arg0, int arg1)
        {
            return false;
        }
    }
    private class BestSequenceSearchTask extends SwingWorker<List<Tuning>, Tuning>
    {
        private BestSequenceCalculator bestSequenceCalculator;
        private long start;
        private double threshold;

        /**
         * Constructor for BestSequenceSearchTask
         * @param bestSequenceCalculator
         * @param threshold 
         */
        public BestSequenceSearchTask (BestSequenceCalculator bestSequenceCalculator, double threshold)
        {
            this.bestSequenceCalculator = bestSequenceCalculator;
            this.threshold = threshold;
        }

        @Override
        protected List<Tuning> doInBackground ()
            throws Exception
        {
            start = System.currentTimeMillis();
            
            List<Tuning> bestSequence = new ArrayList<Tuning> ();
            runButton.setEnabled (false);
            closeButton.setEnabled (false);
            saveReportButton.setEnabled (false);
            applyTuningSequenceButton.setEnabled (false);
            setCursor (Cursor.getPredefinedCursor (Cursor.WAIT_CURSOR));
            Tuning bestTuning = bestSequenceCalculator.getBestTuning ();
            do
            {
                publish (bestTuning);
                bestSequence.add (bestTuning);
                bestTuning = bestSequenceCalculator.getNextTuning ();
            }
            while (bestTuning != null && bestTuning.getUtility () > threshold);
            return bestSequence;
        }

        @Override
        protected void done ()
        {
            setCursor (Cursor.getDefaultCursor ());
            runButton.setEnabled (true);
            saveReportButton.setEnabled (true);
            closeButton.setEnabled (true);
            applyTuningSequenceButton.setEnabled (true);
            try
            {
                bestSequence = get ();
            }
            catch (InterruptedException | ExecutionException e)
            {
                e.printStackTrace ();
            }
            long elapsedSeconds = (System.currentTimeMillis() - start) / 1000;
            JOptionPane.showMessageDialog (null, "The search has finished. ("+ elapsedSeconds / 60 + ":" + elapsedSeconds % 60 +")", "Utility Calculator",
                                           JOptionPane.INFORMATION_MESSAGE);
        }

        @Override
        protected void process (List<Tuning> chunks)
        {
            super.process (chunks);
            DefaultTableModel tableModel = (DefaultTableModel) tuningSequenceTable.getModel ();
            DecimalFormat df = new DecimalFormat ("#.##");
            for (Tuning tuning : chunks)
            {
                if (tuning != null)
                {
                    Object[] row = new Object[] {tuning.getVariable ().getName (),
                            tuning.getState (), df.format (tuning.getUtility ())};
                    tableModel.addRow (row);
                }
            }
            try {
                lblTotalUtility.setText (df.format (bestSequenceCalculator.getGroundUtility ()));
            } catch (IncompatibleEvidenceException e) {
                e.printStackTrace();
            }
        }
    }
    private class NextTuningsSearchTask extends SwingWorker<List<Tuning>, Tuning>
    {
        private BestSequenceCalculator bestSequenceCalculator;

        public NextTuningsSearchTask (BestSequenceCalculator bestSequenceCalculator)
        {
            this.bestSequenceCalculator = bestSequenceCalculator;
        }

        @Override
        protected List<Tuning> doInBackground ()
            throws Exception
        {
            setCursor (Cursor.getPredefinedCursor (Cursor.WAIT_CURSOR));
            runButton.setEnabled (false);
            closeButton.setEnabled (false);
            saveReportButton.setEnabled (false);
            List<Tuning> nextTunings = bestSequenceCalculator.getNextTunings ();
            for (Tuning tuning : nextTunings)
            {
                publish (tuning);
            }
            return nextTunings;
        }

        @Override
        protected void done ()
        {
            setCursor (Cursor.getDefaultCursor ());
            runButton.setEnabled (true);
            saveReportButton.setEnabled (true);
            closeButton.setEnabled (true);
            try
            {
                nextTunings = get ();
            }
            catch (InterruptedException |  ExecutionException e)
            {
                e.printStackTrace ();
            }

        }

        @Override
        protected void process (List<Tuning> chunks)
        {
            super.process (chunks);
            DefaultTableModel tableModel = (DefaultTableModel) nextTuningTable.getModel ();
            DecimalFormat df = new DecimalFormat ("#.##");
            for (Tuning bestTuning : chunks)
            {
                Object[] row = new Object[] {bestTuning.getVariable ().getName (),
                        bestTuning.getState (), df.format (bestTuning.getUtility ())};
                tableModel.addRow (row);
            }
        }
    }
}
