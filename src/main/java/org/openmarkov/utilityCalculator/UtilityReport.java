/*
 * Copyright 2012 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.utilityCalculator;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.utilityCalculator.UtilityCalculator.TuningUtilities;

public class UtilityReport {

	public UtilityReport()
	{
		
	}
	
	public void saveTuningList(String filename, List<Tuning> bestSequence)
        throws IOException
    {
        // create a workbook
        HSSFWorkbook wb = new HSSFWorkbook ();
        // create a sheet
        HSSFSheet sheet = wb.createSheet ("data");
        // Write header
        // Create a row to put the names of the variables
        HSSFRow headerRow = sheet.createRow ((short) 0);
        // Create a row for the sub header
        /* Attributes */        
        HSSFFont font = wb.createFont ();
        font.setFontName("Calibri");
        
        HSSFCellStyle headerStyle = wb.createCellStyle();
        headerStyle.setFont (font);
        headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND );
        headerStyle.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT () .getIndex());
        headerStyle.setAlignment (HSSFCellStyle.ALIGN_CENTER);	
        
        // HEADER
        HSSFCell variableHCell = headerRow.createCell (0, HSSFCell.CELL_TYPE_STRING);
        variableHCell.setCellValue (new HSSFRichTextString ("Variable"));
        variableHCell.setCellStyle (headerStyle);

        HSSFCell valueHCell = headerRow.createCell (1, HSSFCell.CELL_TYPE_STRING);
        valueHCell.setCellValue (new HSSFRichTextString ("State"));
        valueHCell.setCellStyle (headerStyle);

        HSSFCell utilityHCell = headerRow.createCell (2, HSSFCell.CELL_TYPE_STRING);
        utilityHCell.setCellValue (new HSSFRichTextString ("Utility"));
        utilityHCell.setCellStyle (headerStyle);
        
        int rowIndex = 1;
        HSSFCellStyle rowStyle = wb.createCellStyle();
        rowStyle.setFont (font);

        for(Tuning tuningUtility: bestSequence)
        {
        	HSSFRow row = sheet.createRow (rowIndex);        	

        	// Variable
            HSSFCell variableCell = row.createCell (0, HSSFCell.CELL_TYPE_STRING);
            variableCell.setCellValue (new HSSFRichTextString (tuningUtility.getVariable().getName()));
            variableCell.setCellStyle (rowStyle);

            // State
            HSSFCell valueCell = row.createCell (1, HSSFCell.CELL_TYPE_STRING);
            valueCell.setCellValue (new HSSFRichTextString (tuningUtility.getState()));
            valueCell.setCellStyle (rowStyle);        	

            // Utility
            HSSFCell cell = row.createCell (2, HSSFCell.CELL_TYPE_NUMERIC);
            cell.setCellValue (tuningUtility.getUtility());
            cell.setCellStyle (rowStyle);
        	
        	++rowIndex;
        }
        
        //Auto size columns
        for(short i=0; i <= 3; ++i)
        {
            sheet.autoSizeColumn (i);
        }
            
        // dump to file
        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream (filename);
        wb.write (fileOut);
        fileOut.close ();        
        
	}
	
	public void saveAllUtilities(String filename, ArrayList<TuningUtilities> tuningUtilities)
	        throws IOException
	    {
	        // create a workbook
	        HSSFWorkbook wb = new HSSFWorkbook ();
	        // create a sheet
	        HSSFSheet sheet = wb.createSheet ("data");
	        // Write header
	        // Create a row to put the names of the variables
	        HSSFRow headerRow = sheet.createRow ((short) 0);
	        // Create a row for the sub header
	        /* Attributes */        
	        HSSFFont font = wb.createFont ();
	        font.setFontName("Calibri");
	        
	        HSSFCellStyle headerStyle = wb.createCellStyle();
	        headerStyle.setFont (font);
	        headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND );
	        headerStyle.setFillForegroundColor(new HSSFColor.GREY_25_PERCENT () .getIndex());
	        headerStyle.setAlignment (HSSFCellStyle.ALIGN_CENTER);	
	        
	        // HEADER
	        HSSFCell variableHCell = headerRow.createCell (0, HSSFCell.CELL_TYPE_STRING);
	        variableHCell.setCellValue (new HSSFRichTextString ("Variable"));
	        variableHCell.setCellStyle (headerStyle);

	        HSSFCell valueHCell = headerRow.createCell (1, HSSFCell.CELL_TYPE_STRING);
	        valueHCell.setCellValue (new HSSFRichTextString ("State"));
	        valueHCell.setCellStyle (headerStyle);

	        TuningUtilities sampleTuningUtilities = tuningUtilities.get(0);
	        int colIndex = 2;
	        for(Variable variable : sampleTuningUtilities.getUtilities().keySet())
	        {
		        HSSFCell utilityHCell = headerRow.createCell (colIndex, HSSFCell.CELL_TYPE_STRING);
		        utilityHCell.setCellValue (new HSSFRichTextString (variable.getName()));
		        utilityHCell.setCellStyle (headerStyle);
		        ++colIndex;
	        }
	        
	        int rowIndex = 1;
	        HSSFCellStyle rowStyle = wb.createCellStyle();
	        rowStyle.setFont (font);

	        for(TuningUtilities tuningUtility: tuningUtilities)
	        {
	        	HSSFRow row = sheet.createRow (rowIndex);        	

	            // Variable 
	            HSSFCell variableCell = row.createCell (0, HSSFCell.CELL_TYPE_STRING);
	            variableCell .setCellValue (new HSSFRichTextString (tuningUtility.getVariable().getName()));
	            variableCell .setCellStyle (rowStyle);        	

	            // State
	        	HSSFCell valueCell = row.createCell (1, HSSFCell.CELL_TYPE_STRING);
	            valueCell.setCellValue (new HSSFRichTextString (tuningUtility.getState()));
	            valueCell.setCellStyle (rowStyle);        	

		        colIndex = 2;
	            for(Variable variable : sampleTuningUtilities.getUtilities().keySet())
	 	        {
		            // Utility
		            HSSFCell cell = row.createCell (colIndex, HSSFCell.CELL_TYPE_NUMERIC);
		            cell.setCellValue (tuningUtility.getUtilities().get(variable));
		            cell.setCellStyle (rowStyle);
		            ++colIndex;
	 	        }
	        	
	        	++rowIndex;
	        }
	        
	        //Auto size columns
	        for(short i=0; i <= 3; ++i)
	        {
	            sheet.autoSizeColumn (i);
	        }
	            
	        // dump to file
	        // Write the output to a file
	        FileOutputStream fileOut = new FileOutputStream (filename);
	        wb.write (fileOut);
	        fileOut.close ();        
	        
		}	
}
