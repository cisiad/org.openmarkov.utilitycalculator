/*
 * Copyright 2012 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.utilityCalculator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.InvalidStateException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.model.graph.Link;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.Finding;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.ProbNetOperations;
import org.openmarkov.core.model.network.Node;
import org.openmarkov.core.model.network.State;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.operation.LinkRestrictionPotentialOperations;
import org.openmarkov.inference.likelihoodWeighting.LikelihoodWeighting;

public class BestSequenceCalculator {


	private ProbNet probNet;
	private List<Variable> tunableVariables;
	private HashMap<Variable, List<Link<Node>>> linkRestrictions;
	private Variable utilityVariable;
	private double groundUtility = Double.NEGATIVE_INFINITY;

	private EvidenceCase currentEvidence;
	private EvidenceCase groundEvidence;
	private LikelihoodWeighting inferenceAlgorithm = null;
	private int neutralStateIndex = -1;
	
	public BestSequenceCalculator(ProbNet probNet,
			List<Variable> tunableVariables, Variable utilityVariable,
			EvidenceCase currentEvidence, int neutralStateIndex) throws IncompatibleEvidenceException
	{
		this.probNet = probNet;
		this.tunableVariables = ProbNetOperations.sortTopologically(probNet, tunableVariables);
		linkRestrictions = new HashMap<>();
		for(Variable variable: tunableVariables)
		{
			linkRestrictions.put(variable, LinkRestrictionPotentialOperations.getParentLinksWithRestriction(probNet.getNode(variable)));
		}
		
		this.utilityVariable = utilityVariable;
		this.currentEvidence = currentEvidence;
		this.neutralStateIndex = neutralStateIndex;
		this.groundUtility = Double.NEGATIVE_INFINITY;
		
	}

	public LikelihoodWeighting getInferenceAlgorithm(ProbNet probNet) 
	{
		if(inferenceAlgorithm == null)
		{
			try {
				inferenceAlgorithm = new LikelihoodWeighting(probNet);
			} catch (NotEvaluableNetworkException e1) {
				e1.printStackTrace();
			}
			inferenceAlgorithm.setSampleSize(10000);			
		}
		return inferenceAlgorithm;
	}

	public Tuning getBestTuning() throws IncompatibleEvidenceException 
	{
		init();
		return getNextTuning();
	}
	
	public List<Tuning> getBestTunings() throws IncompatibleEvidenceException 
	{
		init();
		return getNextTunings();
	}
	
	public Tuning getNextTuning()
	{
		List<Tuning> tuningList = getNextTunings();
		Tuning nextTuning = null;
		if (!tuningList.isEmpty() && tuningList.get(0).getUtility() > 0.0){
			nextTuning = tuningList.get(0);
			applyTuning(nextTuning);
		}
		return nextTuning;
	}	
	
	@SuppressWarnings("unchecked")
    public List<Tuning> getNextTunings()
	{
        List<Future<Tuning>> tuningFutureList = new ArrayList<Future<Tuning>>();
		List<Tuning> tuningList = new ArrayList<Tuning>();
		int nrOfProcessors = Runtime.getRuntime().availableProcessors();
	    ExecutorService eservice = Executors.newFixedThreadPool(nrOfProcessors);
	    CompletionService < Tuning > cservice = new ExecutorCompletionService < Tuning > (eservice);
	    
	    int tasks = 0;
		for (Variable variableToTest : tunableVariables) {
			for (int i = 0; i < variableToTest.getNumStates(); ++i) {
				if (i != neutralStateIndex) {
					if(!hasRestriction(groundEvidence, variableToTest,  linkRestrictions.get(variableToTest), i))
					{
					    try
                        {
                            tuningList.add (new Propagation (variableToTest, i, groundEvidence, groundUtility).call ());
                        }
                        catch (Exception e)
                        {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
					}
				}
			}
		}
		Collections.sort(tuningList);
		return tuningList;
	}
	
	public List<Tuning> getBestSequence() throws IncompatibleEvidenceException
	{
		List<Tuning> bestSequence = new ArrayList<Tuning>();

		Tuning bestTuning = getBestTuning();
		do
		{
			bestSequence.add(bestTuning);
			bestTuning = getNextTuning();
		}while(bestTuning != null);			
		return bestSequence;		
	}
	
	public void applyTuning(Tuning tuning) {

		{
			groundUtility += tuning.getUtility();

			try {
				groundEvidence.changeFinding(new Finding(tuning.getVariable(),
						tuning.getVariable().getStateIndex(tuning.getState())));
			} catch (InvalidStateException | IncompatibleEvidenceException notGoingToHappen) {
			}
			tunableVariables.remove(tuning.getVariable());
		}
	}
	
	/**
	 * @return the groundUtility
	 * @throws IncompatibleEvidenceException 
	 */
	public double getGroundUtility() throws IncompatibleEvidenceException {
	    if(groundUtility == Double.NEGATIVE_INFINITY)
	    {
	        init();
	    }
		return groundUtility;
	}	
	
	private EvidenceCase getGroundEvidence(EvidenceCase currentEvidence,
			List<Variable> tunableVariables, int neutralStateIndex) {
		EvidenceCase groundEvidence = new EvidenceCase();

		// Calculate ground utility -
		for (Finding finding : currentEvidence.getFindings()) {
			try {
				groundEvidence.addFinding(new Finding(finding.getVariable(),
						finding.getStateIndex()));
			} catch (InvalidStateException | IncompatibleEvidenceException e) {
				// Can not happen
				e.printStackTrace();
			}
		}

		for (Variable tunableVariable : tunableVariables) {
			try {
				if (groundEvidence.contains(tunableVariable)) {
					groundEvidence.changeFinding(new Finding(tunableVariable,
							neutralStateIndex));
				} else {
					groundEvidence.addFinding(new Finding(tunableVariable,
							neutralStateIndex));
				}
			} catch (InvalidStateException | IncompatibleEvidenceException e) {
				// Can not happen
				e.printStackTrace();
			}
		}
		return groundEvidence;
	}
	
	private void init() throws IncompatibleEvidenceException
	{
		LikelihoodWeighting inferenceAlgorithm = getInferenceAlgorithm(probNet);
		groundEvidence = getGroundEvidence(currentEvidence, tunableVariables, neutralStateIndex);
		groundUtility = Double.NEGATIVE_INFINITY;
		inferenceAlgorithm.setPostResolutionEvidence(groundEvidence);
		try {
			groundUtility = inferenceAlgorithm.getProbsAndUtilities().get(utilityVariable).values[0];
		} catch (IncompatibleEvidenceException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	private boolean hasRestriction(EvidenceCase evidence, Variable variable,  List<Link<Node>> links, int nodeStateIndex) {
		State destState = variable.getStates()[nodeStateIndex];
		for (Link<Node> link : links) {
			Variable sourceVar = link.getNode1().getVariable();
			if(evidence.contains(sourceVar))
			{
				if (link.areCompatible(sourceVar.getStates()[evidence.getState(sourceVar)], destState) == 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	private class Propagation implements Callable<Tuning> 
	{
	    private EvidenceCase evidence; 
	    private Variable variableToTest;
	    private int stateIndex;
	    private double groundUtility;
	    
	    public Propagation(Variable variableToTest, int stateIndex,  EvidenceCase groundEvidence, double groundUtility)
	    {
	        this.evidence = new EvidenceCase(groundEvidence);
            try {
                evidence.changeFinding(new Finding(variableToTest, stateIndex));
            } catch (InvalidStateException | IncompatibleEvidenceException e) {
                // Not going to happen
            }
            this.variableToTest = variableToTest;
            this.stateIndex = stateIndex;

	        this.groundUtility = groundUtility;
	    }

        @Override
        public Tuning call ()
            throws Exception
        {
            double utility = Double.NEGATIVE_INFINITY;
            
            inferenceAlgorithm.setPostResolutionEvidence(evidence);
            try {
                utility = inferenceAlgorithm.getProbsAndUtilities()
                        .get(utilityVariable).values[0];
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new Tuning(utility - groundUtility, variableToTest, variableToTest.getStateName(stateIndex));
        }
	}
	
}
